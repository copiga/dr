#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>


/*function declaration*/
int firstopenfiles(char CHinput[],char CHoutput[]);//opens all files specified at startup
void closefiles(void);//closes ALL files flushes all data to outputs too
int semicolons(char delim);//removes semicolons
int linecount(char tocheck);//counts lines
void rewindall(void);//does what it says on the tin, rewinds everything

/*end declarations*/

/*file pointers*/
FILE *Finput;
FILE *Foutput;

/*end of file pointers*/


int main(int argc, char *argv[])
{
  int removedsemis;
  char delim = *argv[3];
  if(firstopenfiles(argv[1],argv[2])<2)
    {
      return 1;
    }
  //  linesbefore = linecount("input");
  removedsemis = semicolons(delim);
  //  linesafter = linecount("output");
  printf ("the operation was successful, %d delimiters were removed from %d lines\nthere are now %d lines\n",removedsemis, linecount('i'), linecount('o'));  
  closefiles();
  return 0;
}

int firstopenfiles(char CHinput[], char CHoutput[])
{
  Finput = fopen(CHinput,"r");
  if(Finput == NULL)
    {
      puts("the input file cannot be read, maybe it doesnt exist\nplease ensure you have the correct parameters");
      printf("the input file was %s\n",CHinput);
      fprintf(stderr,"error opening file %s with attribute \"r\" the pointer is NULL possible file error\n",CHinput);
      return 0;
    }
  Foutput = fopen(CHoutput,"w+");
  if(Foutput == NULL)
    {
      puts("the output file cannot be written, maybe it doesnt exist\nplease ensure you have the correct permissions");
      printf("the output file was %s\n",CHoutput);
      fprintf(stderr,"error opening file %s with attribute \"w\" the pointer is NULL possible file error\n",CHoutput);
      return 1;
    }
  return 2;
}

void closefiles(void)
{
  fclose(Finput);
  fflush(Foutput);
  fclose(Foutput);
}


int semicolons(char delim)
{
  rewindall();
  int sneakychar, semicolons = 0;
  while((sneakychar = fgetc(Finput))!=EOF)
    {
      if(sneakychar == delim)
	{
	  sneakychar = '\n';
	  semicolons++;
	}
      fprintf(Foutput,"%c", sneakychar);
    }
  rewindall();
  return semicolons;
  
}

int linecount(char tocheck)
{
  int sneakychar, lines = 1;
  rewindall();
  if(tocheck == 'i')
    { 
      while((sneakychar = fgetc(Finput)) != EOF)
	{
	  if(sneakychar == '\n')
	    {
	      lines++;
	    }
	}
    }
  else if(tocheck == 'o')
    {
      while((sneakychar = fgetc(Foutput)) != EOF)
	{
	  if(sneakychar == '\n')
	    {
	      lines++;
	    }
	}
    }
  rewindall();
  return lines;
  
}

void rewindall(void)
{
  rewind(Finput);
  rewind(Foutput);
}
